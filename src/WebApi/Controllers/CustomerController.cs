using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Database;

namespace WebApi.Controllers
{
	[Route("customers")]
	public class CustomerController : Controller
	{
		[HttpGet("{id:long}")]
		public async Task<IActionResult> GetCustomerAsync([FromRoute] long id)
		{
			using (var db = new Context())
			{
				var cust = await db.Customers.FindAsync(id);
				if (cust == null)
				{
					return NotFound();
				}
				return Ok(new WebApi.Models.Customer
				{
					Lastname = cust.Lastname,
					Firstname = cust.Firstname,
					Id = cust.Id,
				});
			}
		}

		[HttpPost("")]
		public async Task<IActionResult> CreateCustomerAsync([FromBody] WebApi.Models.Customer customer)
		{
			using (var db = new Context())
			{
				if (await db.Customers.FindAsync(customer.Id) != null)
				{
					return Conflict();
				}

				var cust = new Customer
				{
					Lastname = customer.Lastname,
					Firstname = customer.Firstname,
				};
				var id = await db.Customers.AddAsync(cust);
				await db.SaveChangesAsync();
				return Ok(id.Entity.Id);
			}
		}
	}
}