﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WebClient
{
	static class Program
	{
		private static List<string> names = new List<string>
		{
			"Gregory",
			"Ilya",
			"Mikhail",
			"Yuri",
			"Igor",
};

		private static List<string> surNames = new List<string>
		{
			"Chernishov",
			"Orel",
			"Timofeev",
			"Elizarov",
			"Shilov",
		};

		static Task Main(string[] args)
		{
			Console.WriteLine("Enter customer id");
			if (Int64.TryParse(Console.ReadLine(), out var id))
			{
				Console.WriteLine("Customer from db");
				GetCustomerAndPrint(id);
			}

			var randomCustomerId = SetCustomerAndReturnId(RandomCustomer());
			Console.WriteLine("Randomly created customer from db");
			GetCustomerAndPrint(randomCustomerId);

			Console.ReadKey();
			return Task.CompletedTask;
		}

		private static Customer RandomCustomer()
		{
			var rand = new Random();

			return new Customer
			{
				Lastname = surNames[rand.Next(0, 5)],
				Firstname = names[rand.Next(0, 5)],
			};
		}

		private static void GetCustomerAndPrint(long id)
		{
			var baseAddress = new Uri("http://localhost:5000/");

			var response = new HttpClient()
				.GetAsync(new Uri(baseAddress, $"customers/{id}")).Result;

			if (response.StatusCode == HttpStatusCode.OK)
			{
				var content = response.Content.ReadAsStringAsync().Result;
				var customer = JsonSerializer.Deserialize<Customer>(content);
				Console.WriteLine($"{customer.Id}: {customer.Firstname} {customer.Lastname}");
			}
			else
			{
				Console.WriteLine($"Code {response.StatusCode}");
			}
		}

		private static long SetCustomerAndReturnId(Customer request)
		{
			var baseAddress = new Uri("http://localhost:5000/");

			var response = new HttpClient()
				.PostAsJsonAsync(
					new Uri(baseAddress, "customers"),
					request).Result;

			return Int64.Parse(response.Content.ReadAsStringAsync().Result);
		}
	}
}