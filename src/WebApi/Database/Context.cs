﻿using Microsoft.EntityFrameworkCore;

namespace WebApi.Database
{
	public class Context : DbContext
	{
		public Context()
		{
			Database.EnsureCreated();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder options)
		{
			options.UseSqlite("Filename=Customers.db");
		}

		public DbSet<Customer> Customers { get; set; }
	}
}